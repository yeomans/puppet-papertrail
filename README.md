# Puppet-Papertrail

Puppet-Papertrail is a Puppet module that manages logging to [Papertrail](https://papertrailapp.com/) 
via  using TLS-encrypted rsyslog over TCP.  The module implements the [directions](http://help.papertrailapp.com/kb/configuration/encrypting-remote-syslog-with-tls-ssl/) 
provided by Papertrail.

This class is written to work with Debian Jessie (and thus systemd) and Puppet 4.


## class papertrail

Include this class to start logging to Papertrail.  This class requires the nmap package for ncat
(see (http://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-systemd/)).  
The dependency on nmap is expressed as a realized virtual resource.

This module expects rsyslog, but does not depend on a Puppet rsyslog class.

### Parameters
`$address`: the address provided with your Papertrail account.

`$port`: the port provided with your Papertrail account.

`$pem_file='/etc/papertrail-bundle.pem'`: location of the papertrail-bundle.pem file.
The class downloads papertrail-bundle.pem from `$pem_url` if the file does not already exist, 
or if the MD5 hash of the file differs from `$pem_file_hash`.

`$pem_file_hash`: the MD5 hash of papertrail-bundle.pem found on the 
[directions](http://help.papertrailapp.com/kb/configuration/encrypting-remote-syslog-with-tls-ssl/) 
resource. I  think it best for you to grab this value yourself and supply it.  If Papertrail 
changes their .pem file, you can grab the new hash and update your configuration, causing 
the class to download a new file

`$pem_url='https://papertrailapp.com/tools/papertrail-bundle.pem'`: the URL of the 
papertrail-bundle.pem file.

`$syslog_service='rsyslog'` the name of the logging service . If the papertrail configuration
is changed (via changes to `$address`, `$port`, or `$pem_file`), the service is restarted
using systemctl.

`$syslog_conf_dir='/etc/rsyslog.d'`: the path of the conf.d directory in which to put papertrail configuration.
If you override the default value, it's up to you to ensure the existence of the directory.


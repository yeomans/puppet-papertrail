class papertrail
    (
    $address, 
    $port, 
    $pem_file='/etc/papertrail-bundle.pem',
    $pem_file_hash,
    $pem_url='https://papertrailapp.com/tools/papertrail-bundle.pem',
    $syslog_service='rsyslog', 
    $syslog_conf_dir='/etc/rsyslog.d'
    )
    {

    # nmap needs to be installed to get ncat for use with papertrail.
    realize Package['nmap']
        
    exec
        {
        "wget --quiet --output-document '$pem_file' '$pem_url'":
        path => $::path,
        unless => "echo '$pem_file_hash $pem_file' | md5sum --check  --status -"
        } ->
    exec
        {
        # ensure that the hash is what it should be.
        "echo '$pem_file_hash $pem_file' | md5sum --check  --status -":
        path => $::path,
        }
    
    file
        {
        '/etc/systemd/system/papertrail.service':
        ensure => file, 
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => template('papertrail/papertrail.service.erb'),
        } ~>
    exec
        {
        'enable_papertrail_service':
        command => "systemctl enable /etc/systemd/system/papertrail.service",
        path => $path, 
        refreshonly => true,
        } ~>
    exec
        {
        'start_papertrail_service':
        command => "systemctl start papertrail.service",
        path => $path, 
        refreshonly => true,
        }
        
    file
        {
        "$syslog_conf_dir/papertrail.conf":
        ensure => file, 
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => template('papertrail/rsyslog-papertrail.conf.erb'),
        } ~> 
    exec
        {
        'restart_rsyslog':
        command => "systemctl restart $syslog_service",
        path => $path, 
        refreshonly => true,
        }
    }